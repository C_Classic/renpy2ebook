# renpy2ebook

This application allows you to convert [Ren'Py](http://www.renpy.org/ "Ren'Py Homepage") visual novels into a format which your e-book-reader understands.

## Important note
The current version only allows very basic conversion of mostly simple novels. (Actually I only tested it with [Yandere-Chan](http://games.renpy.org/game/yandere.shtml "Game on games.renpy.org"). You could call this a proof-of-concept application.

## How to use (Linux only)
1. Install the gnu-g++ toolchain, python and latex
1. Checkout the repository
1. Open a terminal, cd into the renpy2ebook directory and enter "make"
1. Locate and copy the game-folder into the programs directory
1. Execute mkDoc (This might take some time)
1. Execute renpy2ebook with the required parameters (see example)
1. Execute mkTex twice (to get cross-references right)
1. Open main.pdf to see your result

### Example for creating a novel (Yandere-Chan)
* ./renpy2ebook -t "Yandere-Chan" -a "Zero-Q" -m .5em
* ./mkTex && ./mkTex

### Download of a finished pdf
* For e-book-readers (pdf): [MEGA](https://mega.co.nz/#!eFZygArQ!dVJf7loDDX1SD2hUHxHzIVhG0g9LreofTBHvFXsSGtw "Download kindle-version from MEGA")
* For PC (pdf): [MEGA](https://mega.co.nz/#!WNAXkZ5B!B36hB0KmqEdNwLPd2XejPxJMF39eJaycUT9sFbGMr_k "Download pc-version from MEGA")

## Some notes
* To see a full list of commands, enter "renpy2ebook --help"
* After you executed renpy2ebook, you can edit the files in ./tex/ to suit your purpose or to correct mistakes.
* Please let me know what you think about this project! Is it a good idea or is it completely useless?
* Want to contribute or have any comments? Feel free to contact me via [E-Mail](mailto:CClassicVideos@aol.com "CClassicVideos@aol.com").
