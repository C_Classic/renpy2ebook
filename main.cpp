/*
 *  renpy2ebook - A program to convert Ren'Py visual novels into ebook-files to be read on e-book-readers..
 *  Copyright (C) 2013  C_Classic <CClassicVideos@aol.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License Version 3 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/*
 * INFO FOR DEVELOPERS:
 * ====================
 * Games used to test this application:
 *  - Yandere-Chan (http://games.renpy.org/game/yandere.shtml)
 *
 * To search for places where I was uncertain search the sourcecode for "TODO:"
 *
 */

/*
 * INFO FOR PEOPLE USING THE PROGRAM
 * Execute the mkDoc script once before using this program.
 *
*/

/*
 * MY OWN TODO LIST
 * - execute latex twice to get cross references right
 * - messsage of "input" is missing
 * - when printing sprites: 16.5 is not always correct!!
 * - when multiple characters appear onto a scene, it always creates a new page (this is bad)
 * - support for multiple characters is still missing
 * - when jumping, scene information gets lost (it resumes with the one from the arc before)
 */


#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

// -- CONFIG
// Input files
#define FILE_SCRIPT "game/script.rpy"
// Output files
#define FILE_MAIN "tex/main.tex"
#define FILE_VARS "tex/vars.tex"
#define FILE_CONF "tex/config.tex"


// -- CODE

// defs
#define SLASH "/" // May be changed for windows compatibility

// For character- and imagelists
#define TAG 0
#define NAME 1

// For sprites
#define PERSON 0
#define IID 1

// Latex: addText
#define SPEECH 0
#define THOUGHTS 1

#define LATEX_VARPREFIX "renpyVAR"


// functions
int parse(string command);
string fixSaid(string input);
string getCharacter(string tag);
string getImage(string tag);
void replaceAll(std::string& str, const std::string& from, const std::string& to);
#define hasArg(arg) (getArg(argc, argv, arg) != "")
string getArg(int argc, char *argv[], const char *search);
void printHelp(char *pname);
void printLicense();
// 2 create latex-code:
void latex_addText(int type, string person, string text);
void latex_addVar(string name, string content);
void latex_setScene(string imageId);
void latex_addLabel(string name);
void latex_addMenuItem(string label, string text);
void latex_addSystem(string text);
void latex_addMenuEnd();
void latex_addSprite(string person, string imageId);
void latex_removeSprite(string person);

// variables
int line = 0;
vector< vector<string> > characters; // tag, name
string username = "";
string title = "";
string author = "";
vector< vector<string> > images; // imageId, path
vector< vector<string> > sprites; // tag, imageId
string lastScene="";

ofstream output;
ofstream varsfile;
ofstream config;

// flags
bool showDidntHit = true;

// 2 create latex:
string lastName = ""; // Used to prevent doubling headings (names)
int lastType=SPEECH;

#define addCharacter(tag, name) characters.push_back(vector<string>(2)); characters[characters.size()-1][TAG] = tag; characters[characters.size()-1][NAME] = name;
#define addSprite(tag, iId) sprites.push_back(vector<string>(2)); sprites[sprites.size()-1][PERSON] = tag; sprites[sprites.size()-1][IID] = iId;
#define addImage(tag, name) images.push_back(vector<string>(2)); images[images.size()-1][TAG] = tag; images[images.size()-1][NAME] = name;
#define removeLeadingSpaces(varName) while(!varName.empty() &&  varName.at(0) == ' ') { varName = varName.substr(1,varName.length());}
#define removeTrailingSpaces(varName) while(!varName.empty() && varName.at(varName.length()-1) == ' ') { varName = varName.substr(0,varName.length()-1);}


int main(int argc, char *argv[])
{
    // Sys vars
    string temp;
    ifstream input;
    string filename;
    // User Vars
    string basePath = "";
    string margin = "";

    // Hello :)
    cout << "\nRen'Py 2 EBook converter version 0.1 beta\n" <<
              "=========================================\n" <<
            "  Copyright © 2013 C_Classic <CClassicVideos@aol.com>\n" <<
            "  This program comes with ABSOLUTELY NO WARRANTY; Type '--license' for details.\n" << endl;

    if (hasArg("--help")) {
        printHelp(argv[0]);
    }
    else if (hasArg("--license")) {
        printLicense();
    }
    else {

        if (hasArg("--no-mishit")) {
            showDidntHit = false;
        }

        basePath = getArg(argc, argv, "-p");
        if (basePath == "") basePath = ".";

        margin = getArg(argc, argv, "-m");
        if (margin == "") margin = "4em";

        username = getArg(argc, argv, "-n");
        if (username == "") username = "Akira";  // (1-明, 2-亮): Japanese unisex name meaning bright or clear. (http://www.20000-names.com/female_japanese_names.htm)

        title = getArg(argc, argv, "-t");
        if (title == "") title = "Specify the title by using -t";

        author = getArg(argc, argv, "-a");
        if (author == "") title = "Specify the author by using -a";
    }


    if (basePath == "") return 1;


    // Open main output file
    filename = basePath + SLASH + string(FILE_MAIN);
    output.open(filename.c_str());
    if (!output.is_open()) {
        cerr << "[ERR]\tCouldn't open " << filename << endl;
        return 1;
    }
    // Write header
    output << "\\input {tex/vars}\n\\input {tex/head}\n"; // --LATEX

    // Open vars file
    filename = basePath + SLASH + string(FILE_VARS);
    varsfile.open(filename.c_str());
    if (!varsfile.is_open()) {
        cerr << "[ERR]\tCouldn't open " << filename << endl;
        return 1;
    }


    // Open config file
    filename = basePath + SLASH + string(FILE_CONF);
    config.open(filename.c_str());
    if (!config.is_open()) {
        cerr << "[ERR]\tCouldn't open " << filename << endl;
        return 1;
    }
    config << "\\usepackage[margin=" << margin << "]{geometry}" << endl;
    config << "\\title {" << title << "}\n" << "\\author {" << author << "}" << endl;

    // General init
    addCharacter("me", username);
    latex_addVar("me", username);


    // Open input and start parsing
    filename = basePath + SLASH + string(FILE_SCRIPT);
    input.open(filename.c_str());
    if (input.is_open()) {
        while (getline(input, temp)) { // Loop through the file
            line++;

            replaceAll(temp, "\r", "");




            removeLeadingSpaces(temp); // Remove this part later? Maybe it would make it easier to parse if.
            removeTrailingSpaces(temp);


            if (temp.length() > 1) { // Hope we won't be omitting any commands because of > 1
                if (parse(temp)) { //something bad happened
                    cerr << "[WARN]\t@line " << line << ": Command not recognized (ignored)" << endl;
                }
            }
            //if (line == 500) break; //TODO: !!!! Only for debugging !!!!
        }
        input.close();
    }
    else {
        cerr << "[ERR]\tCouldn't open " << filename << endl;
    }

    // Write footer
    output << "\\include{tex/tail}"; // --LATEX
    output.close();
    varsfile.close();
    config.close();

    return 0;
}

#define currentCharacter getCharacter(command.substr(0,command.find(" ")))

#define NORMAL 0
#define MENU 1
int parseMode = NORMAL;
string menuText = "";

int parse(string command) {
    if (parseMode == NORMAL) {
        if (command.at(0) == '#') { // comment
             cout << "[INFO]\tComment: " << command.substr(1,command.length()-1) << endl;
        }
        else if (command.at(0) == '\"') { // Thoughts
            command = command.substr(1,command.find_last_of("\"")-1); // Remove quotation marks
            command = fixSaid(command);
            cout << "[INFO]\tProtagonists thoughts: \"" << command << "\"" << endl;

            latex_addText(THOUGHTS, "",command);
        }
        else if (command.find(" = Character") != string::npos) { // Character definition: define tag = Character ('name')
            addCharacter(command.substr(command.find("define ")+7, command.find(" =") - (command.find("define ")+7)), // tag
                         command.substr(command.find("\'")+1, command.find_last_of("\'")-(command.find("\'")+1))); // name
            latex_addVar(LATEX_VARPREFIX + characters[characters.size()-1][0], characters[characters.size()-1][1]);
            cout << "[INFO]\tAdded character \"" << characters[characters.size()-1][1] << "\" with tag \"" << characters[characters.size()-1][0] << "\"" << endl;
        }
        else if (getCharacter(command.substr(0,command.find(" "))) != "") { // A character sais something
            cout << "[INFO]\t" << getCharacter(command.substr(0,command.find(" "))) << " sais \"" << fixSaid(command.substr(command.find("\"")+1, command.find_last_of("\"")-(command.find("\"")+1))) << "\"" << endl;
            latex_addText(SPEECH, currentCharacter, fixSaid(command.substr(command.find("\"")+1, command.find_last_of("\"")-(command.find("\"")+1))));


        }
        else if (command.substr(0, command.find(" ")) == "image") { // Image definition: image tag = "name"
            addImage(command.substr(command.find("image ")+6, command.find(" =") - (command.find("image ")+6)), // tag
                         command.substr(command.find("\"")+1, command.find_last_of("\"")-(command.find("\"")+1))); // name
            cout << "[INFO]\tAdded image \"" << images[images.size()-1][1] << "\" with tag \"" << images[images.size()-1][0] << "\"" << endl;
        }
        else if (command.substr(0, command.find(" ")) == "scene") {
            cout << "[INFO]\tChanging scene to " << command.substr(command.find("image ")+6, command.length()-(command.find("image ")+6)) << endl;
            latex_setScene(command.substr(command.find("image ")+7, command.length()-(command.find("image ")+7)));
        }
        else if (command.substr(0, command.find(" ")) == "label") {
            cout << "[INFO]\tAdding label " << command.substr(command.find("label ")+6, command.find_last_of(":")-(command.find("label ")+6)) << endl;
            latex_addLabel(command.substr(command.find("label ")+6, command.find_last_of(":")-(command.find("label ")+6)));
        }
        else if (command.substr(0, command.find(" ")) == "return") {
            cout << "[INFO]\tFinished arc." << endl;
            latex_addSystem("-- Reached end of arc. --");
        }
        else if (command.substr(0, command.find(" ")) == "menu:") {
            cout << "[INFO]\tChanging mode to menu." << endl;
            parseMode = MENU;
        }
        else if (command.substr(0, command.find(" ")) == "jump") {
           cout << "[INFO]\tAdding jump to " << command.substr(command.find("jump ")+5, command.length()-(command.find("jump ")+5)) << endl;
           latex_addMenuItem(command.substr(command.find("jump ")+5, command.length()-(command.find("jump ")+5)), "Click here to continue reading...");
           latex_addMenuEnd();
        }
        else if (command.substr(0, command.find(" ")) == "show") { //show sprite
            command = command.substr(command.find("show ")+5, command.length()-(command.find("show ")+5));
           // cout << "[INFO]\tShowing sprite " << command.substr(command.find(" ")+1, command.find_last_of(" ")-(command.find(" ")+1)) << " of " << command.substr(0,command.find(" ")) << "." << endl;
            latex_addSprite(command.substr(0,command.find(" ")),
                            command.substr(command.find(" ")+1, command.find_last_of(" ")-(command.find(" ")+1)));
        }
        else if (command.substr(0, command.find(" ")) == "hide") { //show sprite
            command = command.substr(command.find("hide ")+5, command.length()-(command.find("hide ")+5));
            cout << "[INFO]\tHiding sprite " << command.substr(command.find(" ")+1, command.find_last_of(" ")-(command.find(" ")+1)) << " of " << command.substr(0,command.find(" ")) << "." << endl; //TODO: it's not just hiding the certain state
            latex_removeSprite(command.substr(0,command.find(" ")));
        }
        else {
            return 1;
        }
    }
    else {
         if (command.at(0) == '\"') { // Menu item text
             command = command.substr(1,command.find_last_of("\"")-1); // Remove quotation marks
             command = fixSaid(command);
             cout << "[INFO]\tSetting menu text to \"" << command << "\"" << endl;
             menuText = command;
         }
         else if (command.substr(0, command.find(" ")) == "jump") {
            cout << "[INFO]\tAdding menu jump to " << command.substr(command.find("jump ")+5, command.length()-(command.find("jump ")+5)) << endl;
            latex_addMenuItem(command.substr(command.find("jump ")+5, command.length()-(command.find("jump ")+5)), menuText);
         }
         else {
             //TODO: not a good idea
             parseMode=NORMAL;
             cout << "  [INFO]\tChanging mode to normal." << endl;
             latex_addMenuEnd();
             parse(command);
             return 1;
         }
    }
    return 0;
}

string getCharacter(string tag) {
    for (int x=0; x < characters.size(); x++) {
        if (tag == characters[x][TAG]) return characters[x][NAME];
    }
    return "";
}
string getImage(string tag) {
    // Built in images (black)
    if (tag == "black") {
        return "../img/black.png";
    }

    // Custom images
    for (int x=0; x < images.size(); x++) {
        //cout << "[DEBG]\tComparing -" << tag << "- against -" << images[x][TAG] << "-" << endl;
        if (tag == images[x][TAG]) return images[x][NAME];
    }
    cerr << "[ERR]\t@line " << line << ": Couldn't find image for tag \"" << tag << "\"" << endl;
    return "";
}


// FROM: http://stackoverflow.com/questions/3418231/c-replace-part-of-a-string-with-another-string
void replaceAll(std::string& str, const std::string& from, const std::string& to) {
    if(from.empty())
        return;
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != std::string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
    }
}

string fixSaid(string input) {
    string temp="";
    bool flag_write=true;
    for (int x=0; x < input.length(); x++) {
        if (input.at(x) == '{') flag_write = false; // Remove additional commands inside of the text
        if (flag_write) temp.push_back(input.at(x)); // Assemble the new string
        if (input.at(x) == '}') {
            flag_write = true; // Allow writing again
            temp.append(" "); // looks nicer
        }

    }
    // Adapt variables
    if (temp.find("%(") != string::npos) {
        replaceAll(temp,"%(", string("\\") + LATEX_VARPREFIX);
        replaceAll(temp, ")s", ""); //TODO: Maybe other types have other characters? not the s ; TODO: Maybe add a space after it (If there's a character right after it LaTeX might fail to replace it)
    }
    // Some other fixed
    replaceAll(temp, "\"", "''");
    return temp;
}


void printHelp(char *pname) {
    cout << "usage: " << pname << " [options] [-p <path>] [-n <name>] [-m <margin>]\n" << endl;
    cout << "OPTIONS:\n" <<
            "  --help\t\tShows this screen.\n" <<
            "  --license\t\tShows the licensing information.\n" <<
            "  --no-mishit\t\tDon't include the you-didn't-hit-the-button-pages.\n" <<
            "  -p [path]\t\tSpecifies the working directory.\n" <<
            "  -n [name]\t\tSpecifiy the player's name.\n" <<
            "  -t [title]\t\tSpecify the title of the game.\n" <<
            "  -a [author]\t\tSpecify the author.\n"
            "  -m [margin]\t\tSets the page margin.\n" <<
            "             \t\tExample: -m 1.5em\n" <<
            endl;
}

void printLicense() {
    cout << "GNU GPL V3\n" <<
            "----------" << endl;
    cout << "\tThis program is free software: you can redistribute it and/or modify\n" <<
            "\tit under the terms of the GNU General Public License Version 3 as\n" <<
            "\tpublished by the Free Software Foundation.\n\n" <<
            "\tThis program is distributed in the hope that it will be useful,\n" <<
            "\tbut WITHOUT ANY WARRANTY; without even the implied warranty of\n" <<
            "\tMERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n" <<
            "\tGNU General Public License for more details.\n\n" <<
            "\tYou should have received a copy of the GNU General Public License\n" <<
            "\talong with this program.  If not, see <http://www.gnu.org/licenses/>." << endl;
}

string getArg(int argc, char *argv[], const char *search) {
    for (int x=1; x < argc; x++) {
        if (string(argv[x]) == string(search)) {
            if (x+1 >= argc) {
                return "NULL";
            }
            else {
                return argv[x+1]; //TODO: evil stuff might happen here (buffer overflow)
            }
        }
    }
    return "";
}



// LaTeX - wohooo!
void latex_addText(int type, string person, string text) {
    if (type == SPEECH) {
        if (lastType == THOUGHTS) output << "\\par\\bigskip" << endl;
        if (lastName != person) {
            output << "\n\\noindent\\hangindent=1em\\textsc{" << person << "}\\hspace{.2em}" << endl;
        }
        output << "\t" << text << endl;
    }
    else {
        if (lastType != THOUGHTS) output << "\n\\par\\bigskip";
        output << "\t\\noindent\\textit{" << text << "}" << endl;
    }
    lastType = type;
    lastName = person;

    /*
       if (lastName != currentCharacter) {
            output << "\\textbf{" << currentCharacter << "}\\linebreak\n"; // --LATEX
            lastName = currentCharacter;
        }
        output << "\t\\framebox[\\textwidth]{" << fixSaid(command.substr(command.find("\"")+1, command.find_last_of("\"")-(command.find("\"")+1))) << "}" << endl; // --LATEX
     */
}

void latex_addVar(string name, string content) {
    // fix varname
    for (int i=0; i < name.length(); i++) {
        for (int x=48; x <58; x++) {
            if (name[i] == x) name[i] += 16; // TODO: This is not clean at all!!
        }
    }


    varsfile << "\\newcommand{\\" << LATEX_VARPREFIX << name << "}{" << content << "}" << endl;
}

void latex_setScene(string imageId) {
    if (imageId != lastScene) { //New scene = characters move away
        sprites.clear(); // TODO: maybe not when "show bg x" was used?
    }

    output << "\n\\raggedbottom\\pagebreak" << endl;
    output << "\\settoheight\\imageheight{\\includegraphics[width=\\textwidth]{rpa/" << getImage(imageId) << "}}" << endl; // Required for sprites

    // print sprites
    for (int x=0; x < sprites.size(); x++) {
        cout << "\t\tPrinting sprite " << sprites[x][PERSON] << endl;
        cout << "sprite:" << sprites[x][IID] << "-->" << getImage(sprites[x][PERSON] + " " + sprites[x][IID]) << "." << endl;
        output << "\\begin{textblock}{0}(50,16.5    )\n" << //TODO: this value is not always correct!
                  "\t\\includegraphics[height=\\the\\imageheight]{rpa/" << getImage(sprites[x][PERSON] + " " + sprites[x][IID]) << "}\n" <<
                  "\\end{textblock}" << endl;

    }
    // print bg
    output << "\\noindent\\includegraphics[width=\\textwidth]{rpa/" << getImage(imageId) << "}" << endl; //TODO: the rpa-folder can also be the game-folder
    // TODO: ^ Maybe replace "\\raggedbottom\\pagebreak" with "\\newpage"

    lastScene = imageId;
}

void latex_addLabel(string name) {
    output << "\n\\raggedbottom\\pagebreak\n\\phantomsection\\label{" << name << "}" << endl;
}

void latex_addMenuItem(string label, string text) {
    output << "\n\\medskip\\noindent\\hyperref[" << label << "]{" << "\\framebox[\\textwidth]{\\Large{" << text << "}}}\\nopagebreak" << endl;
}

void latex_addMenuEnd() { // TODO: This function might make document look bad when printed (but it is safer when using an e-reader to make sure continue reading on the correct page)
    if (showDidntHit) {
        output << "\n\\pagebreak" << endl;
        latex_addSystem("You didn't hit the button!");
        latex_addSystem("Go back one page.");
    }
    output << "\n\\raggedbottom\\pagebreak" << endl;
}

void latex_addSystem(string text) {
    output << "\n\\noindent\\framebox[\\textwidth]{" << fixSaid(text) << "}" << endl;
}

void latex_addSprite(string person, string imageId) {
    if (imageId == "black") { // TODO: workaround, will force scene to black
        person = "bg";
        imageId = "black";
    }
    if (person == "bg") {
        latex_setScene(person + " " + imageId); //TODO: is this right?
        return;
    }


    for (int x =0; x < sprites.size(); x++) {
        cout << "-" << person << ":" << sprites[x][PERSON] << "-" << endl;
        if (person == sprites[x][PERSON]) {
            cout << "\t\tChange sprite of" << person << " to " << getImage(person + " " + imageId) << endl;
            return;
        }
    }

    cout << "\t\tAdd sprite of " << person << " (" << getImage(person + " " + imageId) << ")" << endl;
    addSprite(person, imageId);

    latex_setScene(lastScene);
}

void latex_removeSprite(string person) {
    for (int x =0; x < sprites.size(); x++) {
        if (person == sprites[x][PERSON]) {
            sprites.erase(sprites.begin()+x);
            cout << "\t\tRemoved sprite of" << person << endl;
            latex_setScene(lastScene);
            return;
        }
    }
}
